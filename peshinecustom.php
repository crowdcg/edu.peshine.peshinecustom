<?php

require_once 'peshinecustom.civix.php';

use CRM_Peshinecustom_ExtensionUtil as E;

/**
 * Implements hook_civicrm_config().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_config/
 */
function peshinecustom_civicrm_config(&$config)
{
  _peshinecustom_civix_civicrm_config($config);
}

/**
 * Implements hook_civicrm_xmlMenu().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_xmlMenu
 */
function peshinecustom_civicrm_xmlMenu(&$files)
{
  _peshinecustom_civix_civicrm_xmlMenu($files);
}

/**
 * Implements hook_civicrm_install().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_install
 */
function peshinecustom_civicrm_install()
{
  _peshinecustom_civix_civicrm_install();
}

/**
 * Implements hook_civicrm_postInstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_postInstall
 */
function peshinecustom_civicrm_postInstall()
{
  _peshinecustom_civix_civicrm_postInstall();
}

/**
 * Implements hook_civicrm_uninstall().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_uninstall
 */
function peshinecustom_civicrm_uninstall()
{
  _peshinecustom_civix_civicrm_uninstall();
}

/**
 * Implements hook_civicrm_enable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_enable
 */
function peshinecustom_civicrm_enable()
{
  _peshinecustom_civix_civicrm_enable();
}

/**
 * Implements hook_civicrm_disable().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_disable
 */
function peshinecustom_civicrm_disable()
{
  _peshinecustom_civix_civicrm_disable();
}

/**
 * Implements hook_civicrm_upgrade().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_upgrade
 */
function peshinecustom_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL)
{
  return _peshinecustom_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implements hook_civicrm_managed().
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_managed
 */
function peshinecustom_civicrm_managed(&$entities)
{
  _peshinecustom_civix_civicrm_managed($entities);
}

/**
 * Implements hook_civicrm_caseTypes().
 *
 * Generate a list of case-types.
 *
 * Note: This hook only runs in CiviCRM 4.4+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_caseTypes
 */
function peshinecustom_civicrm_caseTypes(&$caseTypes)
{
  _peshinecustom_civix_civicrm_caseTypes($caseTypes);
}

/**
 * Implements hook_civicrm_angularModules().
 *
 * Generate a list of Angular modules.
 *
 * Note: This hook only runs in CiviCRM 4.5+. It may
 * use features only available in v4.6+.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_angularModules
 */
function peshinecustom_civicrm_angularModules(&$angularModules)
{
  _peshinecustom_civix_civicrm_angularModules($angularModules);
}

/**
 * Implements hook_civicrm_alterSettingsFolders().
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_alterSettingsFolders
 */
function peshinecustom_civicrm_alterSettingsFolders(&$metaDataFolders = NULL)
{
  _peshinecustom_civix_civicrm_alterSettingsFolders($metaDataFolders);
}

/**
 * Implements hook_civicrm_entityTypes().
 *
 * Declare entity types provided by this module.
 *
 * @link https://docs.civicrm.org/dev/en/latest/hooks/hook_civicrm_entityTypes
 */
function peshinecustom_civicrm_entityTypes(&$entityTypes)
{
  _peshinecustom_civix_civicrm_entityTypes($entityTypes);
}

/**
 * Implements hook_civicrm_themes().
 */
function peshinecustom_civicrm_themes(&$themes)
{
  _peshinecustom_civix_civicrm_themes($themes);
}

function peshinecustom_civicrm_searchColumns($objectName, &$headers,  &$values, &$selector)
{

  if ($objectName == 'contribution') {
    foreach ( $values as $id => $value ) {
      $sql = "SELECT name_100, detail_101 FROM civicrm_value_source_14 WHERE id = %1";
      $values[$id]['source'] = CRM_Core_DAO::singleValueQuery( $sql, array( 1 => array( $value['contribution_id'], 'Integer' ) ) );
      $values[$id]['contribution_source'] = $values[$id]['source'];
    }
  }
}

function peshinecustom_civicrm_mosaicoBaseTemplates(&$templates) {
  $templates['eagleton'] = [
    'name' => 'eagleton',
    'title' => 'eagleton',
    'path' => E::url('eagleton/template-eagleton.html'),
    'thumbnail' => E::url('eagleton/edres/_full.png'),
  ];
}
